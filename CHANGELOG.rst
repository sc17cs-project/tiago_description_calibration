^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Changelog for package tiago_description_calibration
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

0.0.4 (2017-11-02)
------------------
* Merge branch 'master' into 'dubnium-devel'
  # Conflicts:
  #   package.xml
* Update package.xml
* Add the pal_configuration_pkg export in the package.xml
* Contributors: AleDF, alessandrodifava

0.0.3 (2017-10-31)
------------------

0.0.2 (2017-10-16)
------------------
* Create the package with the calibration file used by the tiago_description and used by the customer to change the hand-eye calibration
* add README
* Contributors: AleDF
